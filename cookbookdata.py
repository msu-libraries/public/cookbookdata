#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
COPYRIGHT © 2015
MICHIGAN STATE UNIVERSITY BOARD OF TRUSTEES
ALL RIGHTS RESERVED
 
PERMISSION IS GRANTED TO USE, COPY, CREATE DERIVATIVE WORKS AND REDISTRIBUTE
THIS SOFTWARE AND SUCH DERIVATIVE WORKS FOR ANY PURPOSE, SO LONG AS THE NAME
OF MICHIGAN STATE UNIVERSITY IS NOT USED IN ANY ADVERTISING OR PUBLICITY
PERTAINING TO THE USE OR DISTRIBUTION OF THIS SOFTWARE WITHOUT SPECIFIC,
WRITTEN PRIOR AUTHORIZATION.  IF THE ABOVE COPYRIGHT NOTICE OR ANY OTHER
IDENTIFICATION OF MICHIGAN STATE UNIVERSITY IS INCLUDED IN ANY COPY OF ANY
PORTION OF THIS SOFTWARE, THEN THE DISCLAIMER BELOW MUST ALSO BE INCLUDED.
 
THIS SOFTWARE IS PROVIDED AS IS, WITHOUT REPRESENTATION FROM MICHIGAN STATE
UNIVERSITY AS TO ITS FITNESS FOR ANY PURPOSE, AND WITHOUT WARRANTY BY
MICHIGAN STATE UNIVERSITY OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING
WITHOUT LIMITATION THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
A PARTICULAR PURPOSE. THE MICHIGAN STATE UNIVERSITY BOARD OF TRUSTEES SHALL
NOT BE LIABLE FOR ANY DAMAGES, INCLUDING SPECIAL, INDIRECT, INCIDENTAL, OR
CONSEQUENTIAL DAMAGES, WITH RESPECT TO ANY CLAIM ARISING OUT OF OR IN
CONNECTION WITH THE USE OF THE SOFTWARE, EVEN IF IT HAS BEEN OR IS HEREAFTER
ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.
 
Written by Devin Higgins, 2015
(c) Michigan State University Board of Trustees
Licensed under GNU General Public License (GPL) Version 2.
"""

import os
from lxml import etree

class CookbookData():

    def __init__(self, path_to_text):
        self.path = path_to_text
        self.tree = etree.parse(self.path)
        self.filename = os.path.abspath(self.path)

    def GetRegion(self):
        """
        returns list of etree Element class objects
        """
        region_xpath = "/cookbook/@region"
        self.region = self.tree.xpath(region_xpath)
        if self.region == []:
            self.region = ["None"]
        return self.region

    def GetSubregion(self):
        """
        returns list of etree Element class objects
        """
        subregion_xpath = "/cookbook/@subregion"
        self.subregion = self.tree.xpath(subregion_xpath)
        if self.subregion == []:
            self.subregion = ["None"]
        return self.subregion

    def GetEthnicgroup(self):
        """
        returns list of etree Element class objects
        """
        ethnicgroup_xpath = "/cookbook/@ethnicgroup"
        self.ethnicgroup = self.tree.xpath(ethnicgroup_xpath)
        if self.ethnicgroup == []:
            self.ethnicgroup = ["None"]
        return self.ethnicgroup

    def GetDate(self):
        """
        returns list of etree Element class objects
        """
        date_xpath = "/cookbook/meta/dcDate"
        self.date = [x.text for x in self.tree.xpath(date_xpath)]
        if self.date == []:
            self.date = ["None"]
        return self.date

    def GetTitle(self):
        """
        returns list of etree Element class objects
        """
        title_xpath = "/cookbook/meta/dcTitle"
        self.title = [x.text for x in self.tree.xpath(title_xpath)]
        if self.title == []:
            self.title = ["None"]
        return self.title

    def GetIngredients(self):
        """
        returns list of etree Element class objects
        """
        
        ingredient_xpath = "//ingredient"
        self.ingredients = self.tree.xpath(ingredient_xpath)
        if self.ingredients == []:
            self.ingredients = ["None"]
        else:
            self.ingredients = [x.text for x in self.ingredients]

    def GetUniqueIngredients(self):
        self.unique_ingredients = set([x.lower().replace("\n", " ").strip(":;.,!? ") for x in self.ingredients if x is not None])

    def GetCountUniqueIngredients(self):
        self.ingredient_count = len(self.unique_ingredients)
        if self.ingredients == ["None"]:
            self.ingredient_count = 0
        return self.ingredient_count

    def JoinElements(self, element_list):
        return " | ".join(element_list).replace("\t", "").replace("\n", "")

class CookbookDirectory():
    def __init__(self, directory):
        self.directory = directory
    
    def ProcessFilesCsv(self):
        xmlfiles = (x for x in os.listdir(self.directory) if ".xml" in x)
        with open("cookbookdata.tsv", "w") as f:
            f.write("\t".join(["Filename", "Title", "Year", "Region", "Subregion", "Ethnicgroup", "# Unique Ingredients", "Ingredients"]))
            f.write("\n")
            for xmlfile in os.listdir(self.directory):
                a = CookbookData(os.path.join(self.directory, xmlfile))
                a.GetIngredients()
                a.GetUniqueIngredients()
                f.write("\t".join([xmlfile, 
                                a.JoinElements(a.GetTitle()), 
                                a.JoinElements(a.GetDate()),
                                a.JoinElements(a.GetRegion()),
                                a.JoinElements(a.GetSubregion()),
                                a.JoinElements(a.GetEthnicgroup()),
                                str(a.GetCountUniqueIngredients()), 
                                a.JoinElements(a.unique_ingredients),
                                ]).encode('utf8'))
                f.write("\n")
        print "TSV file complete."





