#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
COPYRIGHT © 2015
MICHIGAN STATE UNIVERSITY BOARD OF TRUSTEES
ALL RIGHTS RESERVED
 
PERMISSION IS GRANTED TO USE, COPY, CREATE DERIVATIVE WORKS AND REDISTRIBUTE
THIS SOFTWARE AND SUCH DERIVATIVE WORKS FOR ANY PURPOSE, SO LONG AS THE NAME
OF MICHIGAN STATE UNIVERSITY IS NOT USED IN ANY ADVERTISING OR PUBLICITY
PERTAINING TO THE USE OR DISTRIBUTION OF THIS SOFTWARE WITHOUT SPECIFIC,
WRITTEN PRIOR AUTHORIZATION.  IF THE ABOVE COPYRIGHT NOTICE OR ANY OTHER
IDENTIFICATION OF MICHIGAN STATE UNIVERSITY IS INCLUDED IN ANY COPY OF ANY
PORTION OF THIS SOFTWARE, THEN THE DISCLAIMER BELOW MUST ALSO BE INCLUDED.
 
THIS SOFTWARE IS PROVIDED AS IS, WITHOUT REPRESENTATION FROM MICHIGAN STATE
UNIVERSITY AS TO ITS FITNESS FOR ANY PURPOSE, AND WITHOUT WARRANTY BY
MICHIGAN STATE UNIVERSITY OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING
WITHOUT LIMITATION THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
A PARTICULAR PURPOSE. THE MICHIGAN STATE UNIVERSITY BOARD OF TRUSTEES SHALL
NOT BE LIABLE FOR ANY DAMAGES, INCLUDING SPECIAL, INDIRECT, INCIDENTAL, OR
CONSEQUENTIAL DAMAGES, WITH RESPECT TO ANY CLAIM ARISING OUT OF OR IN
CONNECTION WITH THE USE OF THE SOFTWARE, EVEN IF IT HAS BEEN OR IS HEREAFTER
ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.
 
Written by Devin Higgins, 2015
(c) Michigan State University Board of Trustees
Licensed under GNU General Public License (GPL) Version 2.
"""

from lxml import etree
import networkx as nx
import matplotlib.pyplot as plt
from itertools import permutations
# Not sure how to use this
from py2cytoscape import util as cy
import os

class ProcessCookbook():

    def __init__(self, cookbook_directory):

        self.recipe_xpath = "//recipe"
        self.ingredient_xpath = "p/ingredient"
        self.ingredient_xpath_var = "p/variation/ingredient"
        self.directory = cookbook_directory
        self.G = nx.Graph()
        self._GetFiles()


    def _GetFiles(self):
        xml_files = (f for f in os.listdir(self.directory) if f.endswith(".xml"))
        for xml in xml_files:
            print xml
            self.cookbook_path = os.path.join(self.directory, xml)
            self._GetRecipes()

    def _GetRecipes(self):

        self.tree = etree.parse(self.cookbook_path)
        self.recipes = self.tree.xpath(self.recipe_xpath)
        self._GetIngredients()

    def _GetIngredients(self):

        for recipe in self.recipes:
            self.ingredient_list = []
            self.ingredients_main = recipe.xpath(self.ingredient_xpath)
            self.ingredients_var = recipe.xpath(self.ingredient_xpath_var)
            self.ingredients = self.ingredients_main + self.ingredients_var
            for ingredient in self.ingredients:
                if ingredient.text is not None:
                    self._ProcessIngredient(ingredient)
            self.BuildGraph()  

    def _ProcessIngredient(self, ingredient):
        self.ingredient_list.append(ingredient.text.lower().strip().rstrip(".;,/"))


    def BuildGraph(self):
        self._GenerateNodes()
        self._GenerateEdges()

    def _GenerateNodes(self):

        for ingredient in self.ingredient_list:
            if self.G.has_node(ingredient):
                current_weight = self.G.node[ingredient]["weight"]
                new_weight = current_weight + 1
            else:
                new_weight = 1

            self.G.add_node(ingredient, weight=new_weight)

    def _GenerateEdges(self):

        for combination in permutations(self.ingredient_list, 2):
            # Skip self-connecting edges
            if combination[1] <> combination[0]:
                if self.G.has_edge(combination[0], combination[1]):
                    new_weight = self.G.edge[combination[0]][combination[1]]["weight"] + 1
                else:
                    new_weight = 1
                self.G.add_edge(combination[0], combination[1], weight=new_weight)

    def WriteGexf(self, filepath):
        
        nx.write_gexf(self.G, filepath)

    def WriteTsv(self, filepath):

        with open(filepath, "w") as f:
            for edge in self.G.edges():
                f.write((edge[0]+"\t"+edge[1]+"\n").encode("UTF-8"))



